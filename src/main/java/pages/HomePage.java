package pages;

import wrappers.Annotations;

public class HomePage extends Annotations {
	
	public LoginPage verifyErrorMessage(String data) 
	{
		String errorMessage = driver.findElementById("errorDiv").getText();
		System.out.println(errorMessage);
		if(errorMessage.contains(data))
			System.out.println("Error Message is verified");
		else
			System.out.println("Error Message is incorrect");
		return new LoginPage();
	}
	
	
	public HomePage verifyLoginName(String data) 
	{
		String loginName = driver.findElementByTagName("h2").getText();
		
		if(loginName.contains(data)) {
			System.out.println("Login success");
		}
		else {
			System.out.println("Logged username mismatch");
		}
		return this;
	}
	
	public LoginPage clickLogoutButton() {
		driver.findElementByClassName("decorativeSubmit").click();
		return new LoginPage();
	}
		
	public MyHomePage clickLink()
	{
		driver.findElementByLinkText("CRM/SFA").click();
		return new MyHomePage();
	}	

}
