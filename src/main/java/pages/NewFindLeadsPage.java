package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.support.ui.ExpectedConditions;

import wrappers.Annotations;

public class NewFindLeadsPage extends Annotations {
	
	Set<String> winHandles;
	List<String> windowHandles = new ArrayList<String>();
	
	public NewFindLeadsPage switchToFindLeads()
	{
		winHandles = driver.getWindowHandles();
		windowHandles.addAll(winHandles);
		driver.switchTo().window(windowHandles.get(1));
		return this;
	}
	
	public NewFindLeadsPage enterLeadId(String data)
	{
		driver.findElementByName("id").sendKeys(data);
		return this;
	}
	
	public NewFindLeadsPage clickFindLeadsButton()
	{
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		wait.until(ExpectedConditions.invisibilityOf(driver.findElementByXPath("//div[text()='One Moment...']")));
		return this;
	}
	
	public NewFindLeadsPage clickFirstResultingLeadId()
	{
		driver.findElementByXPath("//table[@class='x-grid3-row-table']//a").click();
		return this;
	}
	
	public MergeLeadsPage switchToMergeLeads()
	{
		driver.switchTo().window(windowHandles.get(0));
		return new MergeLeadsPage();
	}

}
