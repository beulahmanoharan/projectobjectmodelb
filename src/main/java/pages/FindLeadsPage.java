package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import wrappers.Annotations;

public class FindLeadsPage extends Annotations
{
	public static String leadId="";
	public  String firstLeadNam="";
	public FindLeadsPage enterFirstName(String data)
	{
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(data);
		return this;
	}
	
	public FindLeadsPage clickPhoneTab()
	{
		driver.findElementByXPath("//span[text()='Phone']").click();
		return this;
	}
	
	public FindLeadsPage clickEmailTab()
	{
		driver.findElementByLinkText("Email").click();
		return this;
	}
	
	public FindLeadsPage enterCountryCode(String data)
	{
	driver.findElementByName("phoneCountryCode").clear();
	driver.findElementByName("phoneCountryCode").sendKeys("91");
	return this;
	}
	
	public FindLeadsPage enterAreaCode(String data)
	{
	driver.findElementByName("phoneAreaCode").sendKeys("044");
	return this;
	}
	
	public FindLeadsPage enterPhoneNumber(String data)
	{
	driver.findElementByName("phoneNumber").sendKeys("6777899");
	return this;
	}
	
	public FindLeadsPage enterEmailId(String data)
	{
		driver.findElementByName("emailAddress").sendKeys(data);
		return this;
	}
		
	public FindLeadsPage clickFindLeads(){
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		wait.until(ExpectedConditions.invisibilityOf(driver.findElementByXPath("//div[text()='One Moment...']")));
		return this;
	}
	
	public FindLeadsPage fetchLeadId()
	{
		leadId=driver.findElementByXPath("//table[@class='x-grid3-row-table']//a").getText();
		return this;
	}
	
	public String fetchLeadFirstName()
	{	
		WebElement firstLeadName=driver.findElementByXPath("(//table[@class='x-grid3-row-table']//a)[3]");
		 firstLeadNam = firstLeadName.getText();
		System.out.println("First resulting lead Name in the grid..."+firstLeadNam);
		return firstLeadNam;
	}
	
	/*public FindLeadsPage fetchLeadFirstName()
	{	
		WebElement firstLeadName=driver.findElementByXPath("(//table[@class='x-grid3-row-table']//a)[3]");
		firstLeadNam = firstLeadName.getText();
		System.out.println("First resulting lead Name in the grid..."+firstLeadNam);
		return this;
	}*/
	
	public ViewLeadPage clickFirstLeadId()
	{		
		
		driver.findElementByXPath("//table[@class='x-grid3-row-table']//a").click();
		return new ViewLeadPage();
	}	
	
	public ViewLeadPage clickFirstLeadName()
	{				
		WebElement firstLeadName=driver.findElementByXPath("(//table[@class='x-grid3-row-table']//a)[3]");
		firstLeadName.click();
		return new ViewLeadPage();
	}	
	
	
	public FindLeadsPage enterLeadId()
	{
		driver.findElementByName("id").sendKeys(leadId);
		return this;
	}
	
	public FindLeadsPage enterLeadIdFromExcel(String data)
	{
		driver.findElementByName("id").sendKeys(data);
		return this;
	}
	
	public void verifyErrorMessage(String data)
	{
		String errorMsg = driver.findElementByClassName("x-paging-info").getText();
		if(errorMsg.equals(data))
			System.out.println("Error Message shown has been verified");
		else
			System.out.println("Error Message verification fails...");
		
	}

}
