package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import wrappers.Annotations;

public class MergeLeadsPage extends Annotations
{
	public NewFindLeadsPage clickFromLeadIcon()
	{
		driver.findElementByXPath("//input[@id='partyIdFrom']/following::img").click();
		return new NewFindLeadsPage();
	}
	
	public NewFindLeadsPage clickToLeadIcon()
	{
		driver.findElementByXPath("//input[@id='partyIdTo']/following::img").click();
		return new NewFindLeadsPage();
	}
	
	public MergeLeadsPage clickMerge()
	{
		driver.findElementByLinkText("Merge").click();
		return this;
	}
	
	public ViewLeadPage switchToAlert()
	{
		driver.switchTo().alert().accept();
		return new ViewLeadPage();
	}
	
}
