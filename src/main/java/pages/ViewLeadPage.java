package pages;

import wrappers.Annotations;

public class ViewLeadPage extends Annotations
{
	public ViewLeadPage verifyFirstName(String data)
	{
		String firstNam = driver.findElementById("viewLead_firstName_sp").getText();
		
		if(firstNam.equals(data))
			System.out.println("First Name has been verified");
		else
			System.out.println("First Name mismatches");
	
		return this;
	}
	
	public UpdateLeadPage clickEdit()
	{
		driver.findElementByLinkText("Edit").click();
		return new UpdateLeadPage();
	}
	
	public MyLeadsPage clickDelete()
	{
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}
	
	public DuplicateLeadPage clickDuplicateLead()
	{
		driver.findElementByLinkText("Duplicate Lead").click();
		return new DuplicateLeadPage();
	}
	
	
	//To navigate during Merge Lead Test case
	public FindLeadsPage clickFindLeads()
	{
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadsPage();
	}
	
	
	public ViewLeadPage verifyCompanyName(String data)
	{
		String updatedCompanyName = driver.findElementById("viewLead_companyName_sp").getText();
		if(updatedCompanyName.contains(data))
			System.out.println("Updated CompanyName has been verified");
		else
			System.out.println("Updated Company Name is incorrect");
		return this;
	}
	
	public ViewLeadPage verifyDuplicatedLeadName(String data)
	{
		String leadfirstName = driver.findElementById("viewLead_firstName_sp").getText();
		if(data.equals(leadfirstName))
			System.out.println("Captured Lead name is same as duplicated lead name...");
		else
			System.out.println("Captured lead name and duplicated lead name mismatches...");
		return this;
	}
}
