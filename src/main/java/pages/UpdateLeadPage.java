package pages;

import wrappers.Annotations;

public class UpdateLeadPage extends Annotations
{
	public UpdateLeadPage updateCompanyName(String data)
	{
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys(" SG ");
		return this;
	}
	
	public ViewLeadPage clickUpdate()
	{
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage();
	}

}
