package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC005DeleteLeadTest extends Annotations{
	
	@BeforeTest
	public void setData()
	{
		excelFileName="TC005";
	}
	
	@Test(dataProvider="fetchData")
	public void deleteLead(String userName, String password, String countryCode, String areaCode,
			String phoneNum, String errorMsg)
	{
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.clickLink()
		.clickLeads()
		.clickFindLeads()
		.clickPhoneTab()
		.enterCountryCode(countryCode)
		.enterAreaCode(areaCode)
		.enterPhoneNumber(phoneNum)
		.clickFindLeads()
		.fetchLeadId()
		.clickFirstLeadId()
		.clickDelete()
		.clickFindLeads()
		.enterLeadId()
		.clickFindLeads()
		.verifyErrorMessage(errorMsg);
	}
}
