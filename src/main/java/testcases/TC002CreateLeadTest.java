package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC002CreateLeadTest extends Annotations
{
	@BeforeTest
	public void setData()
	{
		excelFileName="TC002";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String uName, String pwd, String cName, String fName, String lName)
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickLink()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLeadButton()
		.verifyFirstName(fName);		
	}
}
