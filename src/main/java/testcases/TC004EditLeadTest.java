package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC004EditLeadTest extends Annotations
{
	@BeforeTest
	public void setData()
	{
		excelFileName="TC004";
	}
	
	@Test(dataProvider="fetchData")
	public void editLead(String userName, String password, String firstName, String companyName)
	{
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.clickLink()
		.clickLeads()
		.clickFindLeads()
		.enterFirstName(firstName)
		.clickFindLeads()
		.clickFirstLeadId()
		.clickEdit()
		.updateCompanyName(companyName)
		.clickUpdate()
		.verifyCompanyName(companyName);
	}
}
