package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC007MergeLeadTest extends Annotations
{
	@BeforeTest
	public void setData()
	{
		excelFileName="TC007";
	}
	
	@Test(dataProvider="fetchData")
	public void mergeLead(String userName, String password, String fromLeadId, String toLeadId, String errorMsg)
	{
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.clickLink()
		.clickLeads()
		.clickMergeLeads()
		.clickFromLeadIcon()
		.switchToFindLeads()
		.enterLeadId(fromLeadId)
		.clickFindLeadsButton()
		.clickFirstResultingLeadId()
		.switchToMergeLeads()
		.clickToLeadIcon()
		.switchToFindLeads()
		.enterLeadId(toLeadId)
		.clickFindLeadsButton()
		.clickFirstResultingLeadId()
		.switchToMergeLeads()
		.clickMerge()
		.switchToAlert()
		.clickFindLeads()
		.enterLeadIdFromExcel(fromLeadId)
		.clickFindLeads()
		.verifyErrorMessage(errorMsg);

	}
}
