package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.FindLeadsPage;
import pages.LoginPage;
import wrappers.Annotations;

public class TC006DuplicateLeadTest extends Annotations{
	
	@BeforeTest
	public void setData()
	{
		excelFileName="TC006";
	}
	
	@Test(dataProvider="fetchData")
	public void duplicateLead(String userName, String password,String emailId)
	{
		String leadFirstName = new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.clickLink()
		.clickLeads()
		.clickFindLeads()
		.clickEmailTab()
		.enterEmailId(emailId)
		.clickFindLeads()
		.fetchLeadFirstName();
		new FindLeadsPage()
		.clickFirstLeadName()
		.clickDuplicateLead()
		.clickCreateLead()
		.verifyDuplicatedLeadName(leadFirstName);
		
	}

}
