package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC003LoginFailure extends Annotations {
	
	@BeforeTest
	public void setData() {
		excelFileName = "TC003";
	}
	
	@Test(dataProvider = "fetchData")
	public void verifyLoginFailure(String userName, String password, String msg) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.verifyErrorMessage(msg);
	}
}	
